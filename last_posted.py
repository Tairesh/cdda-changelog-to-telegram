import os


LAST_PUBLISHED_VERSION_FILE = "changelog_posted.txt"


def get() -> int:
    """
    Get last published version number
    :return: last published version number
    """
    if os.path.isfile(LAST_PUBLISHED_VERSION_FILE):
        with open(LAST_PUBLISHED_VERSION_FILE, 'r') as f:
            return int(f.read())
    else:
        return 0


def save(version: int):
    """
    Save last published version number
    :param version: last published version number
    :return:
    """
    with open(LAST_PUBLISHED_VERSION_FILE, 'w') as f:
        f.write(str(version))
