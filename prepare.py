import html
import re
from datetime import datetime
from typing import List


GITHUB_ISSUE_URL_TEMPLATE = "https://github.com/CleverRaven/Cataclysm-DDA/issues/{}"
GITHUB_RELEASE_URL_TEMPLATE = "https://github.com/CleverRaven/Cataclysm-DDA/releases/tag/cdda-jenkins-b{}"
r_hashtag = re.compile(r'#([\d]+)')


def _prepare(text: str) -> str:
    text = html.escape(text)

    def _hashtag(match):
        number = match.group(1)
        url = GITHUB_ISSUE_URL_TEMPLATE.format(number)
        return f'<a href="{url}">#{number}</a>'

    text = r_hashtag.sub(_hashtag, text)
    return text


def _message_to_bundles(message: str) -> List[str]:
    if len(message) > 2900:
        bundles = []
        rows = message.split('\n')
        cm = ''
        for row in rows:
            if len(cm) + len(row) < 2500:
                cm += row + '\n'
            else:
                bundles.append(cm)
                cm = row + '\n'
        if len(cm):
            bundles.append(cm)
        return bundles
    else:
        return [message]


def get_bundles(builds: list):
    bundles = []
    for build in builds:
        release_url = GITHUB_RELEASE_URL_TEMPLATE.format(build["number"])
        message = f'<a href="{release_url}">Build <b>#{build["number"]}</b></a>' \
                  f', status: [{build["result"]}]' \
                  ', timestamp: ' + datetime.utcfromtimestamp(build["timestamp"] // 1000).strftime('%Y-%m-%d %H:%M')

        if len(build['changes']):
            message += "\nChanges:\n"
            for change in build['changes']:
                message += f"    * {_prepare(change)}\n"
        else:
            message += '\n(no changes)'
        bundles += _message_to_bundles(message)

    return bundles
