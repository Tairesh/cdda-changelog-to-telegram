import json
import urllib.request

import last_posted


API_URL = "https://ci.narc.ro/job/Cataclysm-Matrix/api/json?" \
          "tree=builds[number,timestamp,building,result,changeSet[items[msg,id]],runs[result,fullDisplayName]]&" \
          "xpath=//build&wrapper=builds"


def get_builds(skip_version: int) -> list:
    """
    Get builds from jenkins API
    :param skip_version: last published version. this and earlier versions will be skipped
    :return: list of build objects
    """
    data = json.loads(urllib.request.urlopen(API_URL).read())['builds']
    skip_version = last_posted.get()

    builds = []
    for build in data:
        if build["building"]:
            continue
        if int(build["number"]) <= skip_version:
            break

        builds.append({
            'number': build["number"],
            'timestamp': build["timestamp"],
            'result': build["result"],
            'changes': [item["msg"] for item in build["changeSet"]["items"]]
        })
    return list(reversed(builds))
