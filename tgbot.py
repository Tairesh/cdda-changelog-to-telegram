import json
import traceback

from telebot import apihelper, TeleBot

import last_posted
import prepare


ADMIN = 31445050


def send(builds: list):
    if len(builds) == 0:
        return

    bundles = prepare.get_bundles(builds)

    secrets = json.load(open('secrets.json', 'r'))
    channel = int(secrets['changelogs_channel'])
    if 'proxy' in secrets:
        apihelper.proxy = {'https': secrets['proxy']}
    bot = TeleBot(secrets['token'])

    try:
        for bundle in bundles:
            bot.send_message(channel, bundle, parse_mode='HTML')
        last_posted.save(builds[-1]["number"])
    except Exception:
        bot.send_message(ADMIN, traceback.format_exc())
