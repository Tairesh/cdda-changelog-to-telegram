This script publishes changelogs to selected Telegram channel. 

Configure
=========
Rename `secrets-sample.json` to `secrets.json` and edit it.
```
{
  "token": "Put your Telegram bot token here",
  "changelogs_channel": 12345679, // put Telegram channel, group or user id here
  "proxy": "socks5://user:password@url:port" // specify a proxy if necessary, or delete this field
}
```

Run
===
Install requirements by `pip install -r requirements.txt`
Run `python main.py`